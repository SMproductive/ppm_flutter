import 'dart:async';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:super_clipboard/super_clipboard.dart';
import 'dart:math';
import 'dart:io';

import 'data.dart';

enum WindowType { login, passwordlist, edit, fileerror, save }

// TODO android fingerprint -> store password use in widget
// TODO store default database -> last used one shared_preferences use in widget
// TODO costomize optionsviewbuilder in edit

const double standardWidth = 200;
const double standardHeight = 20;

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Graphical ppm',
      home: const Screen(),
      theme: ThemeData(
          useMaterial3: true,
          colorScheme:
              ColorScheme.fromSeed(seedColor: const Color(0xFF2E3440))),
    );
  }
}

class Screen extends StatefulWidget {
  const Screen({super.key});
  @override
  State<StatefulWidget> createState() => _ScreenState();
}

class _ScreenState extends State<Screen> {
  WindowType selectedWindow = WindowType.login;

  @override
  void initState() {
    super.initState();
  }

  void _nextWindow(WindowType win) {
    setState(() {
      selectedWindow = win;
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget window;
    switch (selectedWindow) {
      case WindowType.login:
        window = Login(next: _nextWindow);
        break;
      case WindowType.passwordlist:
        window = PasswordList(
          next: _nextWindow,
        );
        break;
      case WindowType.fileerror:
        window = FileError(next: _nextWindow);
        break;
      case WindowType.edit:
        window = Edit(next: _nextWindow);
        break;
      case WindowType.save:
        window = Save(next: _nextWindow, error: Data.instance.save());
        break;
      default:
        window = const Text('Error');
    }
    return Scaffold(body: window);
  }
}

class Save extends StatelessWidget {
  const Save({super.key, required this.error, required this.next});

  final Function(WindowType) next;
  final bool error;

  @override
  Widget build(context) {
    return GestureDetector(
      onTap: () {
        next(WindowType.passwordlist);
      },
      child: Container(
        color: error ? Colors.green : Colors.red,
        child: Center(
          child: Text(error ? 'Saved' : 'Error'),
        ),
      ),
    );
  }
}

class Edit extends StatefulWidget {
  const Edit({super.key, required this.next});

  final Function(WindowType) next;

  @override
  State<Edit> createState() => _EditState();
}

class _EditState extends State<Edit> {
  String name = "";
  String password = '';
  double length = 22;

  void _setLength(double len) {
    setState(() {
      length = len;
    });
  }

  void _generatePassword() {
    const String upperCaseLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
    const String digits = "0123456789";
    const String specialCharacters = "!@#\$%^&*()-_=+<>?";

    const String allCharacters =
        upperCaseLetters + lowerCaseLetters + digits + specialCharacters;

    final random = Random.secure();

    String pwd = List.generate(length.toInt(), (index) {
      final randomIndex = random.nextInt(allCharacters.length);
      return allCharacters[randomIndex];
    }).join();
    setState(() {
      password = pwd;
    });
  }

  @override
  Widget build(context) {
    return Center(
        child: FittedBox(
      child: Column(
        children: [
          SizedBox(
            width: standardWidth,
            child: Autocomplete<String>(
              optionsBuilder: (TextEditingValue userInput) {
                if (userInput.text == "") {
                  return const Iterable.empty();
                } else {
                  return Data.instance.getContent().keys.where((String item) =>
                      item.contains(userInput.text.toLowerCase()));
                }
              },
              fieldViewBuilder: (context, controller, focusNode, callback) =>
                  TextField(
                controller: controller,
                focusNode: focusNode,
                onEditingComplete: callback,
                onChanged: (String value) {
                  name = value;
                },
                decoration: const InputDecoration(
                  labelText: "Name",
                  border: OutlineInputBorder(),
                ),
              ),
              onSelected: (String value) {
                name = value;
              },
            ),
          ),
          const SizedBox(
            height: standardHeight,
          ),
          Slider(
            value: length,
            onChanged: (val) {
              _setLength(val);
            },
            label: length.round().toString(),
            divisions: 100,
            max: 100,
            min: 0,
          ),
          const SizedBox(
            height: standardHeight,
          ),
          SizedBox(
            width: standardWidth,
            child: TextField(
              obscureText: true,
              onSubmitted: (_) {
                Data.instance.update(name, password);
                widget.next(WindowType.passwordlist);
              },
              onChanged: (String value) {
                password = value;
              },
              controller: TextEditingController(
                text: password,
              ),
              decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Password',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.password),
                    onPressed: () {
                      _generatePassword();
                    },
                  )),
            ),
          ),
          const SizedBox(
            height: standardHeight,
          ),
          ElevatedButton.icon(
            onPressed: () {
              Data.instance.update(name, password);
              widget.next(WindowType.passwordlist);
            },
            label: const Text('Submit'),
            icon: const Icon(Icons.save),
          ),
        ],
      ),
    ));
  }
}

class PasswordList extends StatelessWidget {
  const PasswordList({
    super.key,
    required this.next,
  });

  final Function(WindowType) next;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      const saveIcon = Icon(Icons.save);
      const addIcon = Icon(Icons.edit);
      const addText = Text('Edit');
      const saveText = Text('Save');
      return Center(
        child: Row(
          children: [
            FittedBox(
              child: Padding(
                padding: const EdgeInsets.all(standardHeight),
                child: Column(
                  children: [
                    constraints.maxWidth > 600
                        ? ElevatedButton.icon(
                            // onLongPress: () async {
                            //   pickFile();
                            //   next(WindowType.save);
                            // },
                            onPressed: () {
                              next(WindowType.save);
                            },
                            label: saveText,
                            icon: saveIcon,
                          )
                        : // GestureDetector(
                          //   onLongPress: () async {
                          //     pickFile();
                          //     next(WindowType.save);
                          //   },
                          //   child:
                            IconButton(
                              onPressed: () {
                                next(WindowType.save);
                              },
                              icon: saveIcon,
                            ),
                          // ),
                    const SizedBox(
                      height: standardHeight,
                    ),
                    constraints.maxWidth > 600
                        ? ElevatedButton.icon(
                            onPressed: () {
                              next(WindowType.edit);
                            },
                            label: addText,
                            icon: addIcon,
                          )
                        : IconButton(
                            onPressed: () {
                              next(WindowType.edit);
                            },
                            icon: addIcon)
                  ],
                ),
              ),
            ),
            Expanded(
                child: ListView(
              children: [
                for (var entry in Data.instance.getContent().entries)
                  Entry(
                    name: entry.key,
                    password: entry.value,
                  ),
              ],
            )),
          ],
        ),
      );
    });
  }
}

class Login extends StatefulWidget {
  const Login({super.key, required this.next});

  final void Function(WindowType) next;

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  String path = '';

  @override
  void initState() {
    super.initState();
    if (Data.instance.file != null) {
      path = Data.instance.file!.path;
    }
  }

  void _setPath(String path) {
    setState(() {
      this.path = path;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FittedBox(
        child: Column(
          children: [
            ElevatedButton.icon(
              onPressed: () {
                pickFile().then((onValue) {
                  if (Data.instance.file != null) {
                    _setPath(Data.instance.file!.path);
                  }
                });
              },
              label: Text(path),
              icon: const Icon(Icons.file_open),
            ),
            const SizedBox(
              height: standardHeight,
            ),
            SizedBox(
              width: standardWidth,
              child: TextField(
                onChanged: Data.instance.setPassword,
                onSubmitted: (_) {
                  widget.next(Data.instance.extractContent()
                      ? WindowType.passwordlist
                      : WindowType.fileerror);
                },
                obscureText: true,
                decoration: const InputDecoration(
                    border: OutlineInputBorder(), labelText: 'Password'),
              ),
            ),
            const SizedBox(
              height: standardHeight,
            ),
            ElevatedButton.icon(
                onPressed: () {
                  widget.next(Data.instance.extractContent()
                      ? WindowType.passwordlist
                      : WindowType.fileerror);
                },
                label: const Text('Next'),
                icon: const Icon(Icons.login))
          ],
        ),
      ),
    );
  }
}

Future<void> pickFile() async {
  FilePickerResult? result = await FilePicker.platform.pickFiles();
  if (result != null) {
    Data.instance.file = File(result.files.single.path!);
  }
}

class FileError extends StatelessWidget {
  const FileError({super.key, required this.next});

  final void Function(WindowType) next;

  @override
  Widget build(context) {
    return Center(
      child: FittedBox(
        child: Column(
          children: [
            const Text('Error opening file!'),
            const SizedBox(
              height: standardHeight,
            ),
            ElevatedButton(
                onPressed: () {
                  next(WindowType.login);
                },
                child: const Text('Try again'))
          ],
        ),
      ),
    );
  }
}

class Entry extends StatefulWidget {
  const Entry({super.key, required this.name, required this.password});

  final String name;
  final String password;

  @override
  State<Entry> createState() => _EntryState();
}

class _EntryState extends State<Entry> {
  bool copied = false;
  String show = "";

  @override
  void initState() {
    super.initState();
    show = widget.name;
  }

  @override
  Widget build(context) {
    return GestureDetector(
      onDoubleTap: _toggleShow,
      child: TextButton(
          onPressed: () {
            _copy(widget.password);
          },
          onLongPress: _reset,
          style: TextButton.styleFrom(
              backgroundColor: copied
                  ? Theme.of(context).primaryColorDark
                  : Theme.of(context).primaryColorLight),
          child: Text(show)),
    );
  }

  void _copy(String item) {
    final clipboard = SystemClipboard.instance;
    if (clipboard == null) {
      return; // Clipboard API is not supported on this platform.
    }
    final item = DataWriterItem();
    item.add(Formats.plainText(widget.password));
    clipboard.write([item]);

    setState(() {
      copied = true;
    });
  }

  void _reset() {
    setState(() {
      copied = false;
    });
  }

  void _toggleShow() {
    setState(() {
      if (show == widget.name) {
        show = widget.password;
      } else {
        show = widget.name;
      }
    });
  }
}
