import 'dart:convert';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';
import 'package:file_picker/file_picker.dart';
import 'dart:io';

const blockSize = 16;
const keySize = 32;

class Data {
  Map<String, String> _content = {};
  Key _key = Key(Uint8List(keySize));
  File? file;

  Data._privateConstructor();
  static Data? _instance;

  static Data get instance {
    _instance ??= Data._privateConstructor();
    return _instance!;
  }

  // TODO check file size -> don't load to big files
  bool extractContent() {
    Uint8List bytes;
    if (file != null) {
      bytes = file!.readAsBytesSync();
      print("content: $bytes");
    } else {
      bytes = Uint8List(0);
    }
    try {
      final ivBytes = bytes.sublist(0, blockSize);
      final content = bytes.sublist(blockSize);

      final iv = IV(ivBytes);
      final encrypter = Encrypter(AES(_key, mode: AESMode.ctr, padding: null));

      final decrypted = encrypter.decrypt(Encrypted(content), iv: iv);
      _content = Map<String, String>.from(jsonDecode(decrypted));
    } catch (e) {
      return false;
    }
    return true;
  }

  Map<String, String> getContent() {
    return _content;
  }

  Uint8List getEncrypted() {
    try {
      final encrypter = Encrypter(AES(_key, mode: AESMode.ctr, padding: null));
      final iv = IV.fromLength(blockSize);
      final jsonString = jsonEncode(_content);

      var bytes = Uint8List(jsonString.length + blockSize);
      final encrypted = encrypter.encrypt(jsonString, iv: iv);
      for (var i = 0; i < bytes.length; i++) {
        bytes[i] = i < blockSize ? iv.bytes[i] : encrypted.bytes[i - blockSize];
      }
      return bytes;
    } catch (e) {
      return Uint8List(0);
    }
  }

  setPassword(String password) async {
    _key = Key(Uint8List.fromList(sha256.convert(utf8.encode(password)).bytes));
  }

  void update(String name, String password) {
    if (name == "") {
      return;
    }
    if (password == "") {
      _content.remove(name);
    } else {
      _content[name] = password;
    }
  }

  bool save() {
    try {
      file!.writeAsBytesSync(getEncrypted());
    } catch (e) {
      return false;
    }
    return true;
  }
}
